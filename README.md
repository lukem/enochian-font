# enochian-font

An Enochian font derived from Sloane MS 3188 and released under the terms of the SIL Open Font License.

## Source Material

Sloane MS 3188, Dr. John Dee's conferences with angels from Dec. 22, 1581 to
May 30, 1583, is available from the British Library:
http://www.bl.uk/manuscripts/FullDisplay.aspx?ref=Sloane_MS_3188

In his 1981 PhD dissertation (University of Birmingham, 1982), JOHN DEE'S
ACTIONS WITH SPIRITS: 22 DECEMBER 1581 TO 23 MAY 1583, Christopher Lionel
Whitby transcribes Sir Frederic Madden's January 1854 flyleaf comment [pages
3-4] stating that this volume is "in Dr. Dee's own handwriting, as far as
fol. 108."

This dissertation is available in two volumes:
* https://etheses.bham.ac.uk/id/eprint/3149/1/Whitby82PhD1.pdf
* https://etheses.bham.ac.uk/id/eprint/3149/2/Whitby82PhD2.pdf

On folio 104, Dee has written out the Enochian alphabet:

<img src="images/MS3188-font-103v-104r.jpg">

## Methods

I extracted each of the letters into separate files:
* Un (A) <img src="images/MS3188-A-Un.jpg">
* Pa (B) <img src="images/MS3188-B-Pa.jpg">
* Veh (C) <img src="images/MS3188-C-Veh.jpg">
* Gal (D) <img src="images/MS3188-D-Gal.jpg">
* Graph (E) <img src="images/MS3188-E-Graph.jpg">
* Or (F) <img src="images/MS3188-F-Or.jpg">
* Ged (G) <img src="images/MS3188-G-Ged.jpg">
* Na (H) <img src="images/MS3188-H-Na.jpg">
* Gon (I) <img src="images/MS3188-I-Gon.jpg">
* Ur (L) <img src="images/MS3188-L-Ur.jpg">
* Tal (M) <img src="images/MS3188-M-Tal.jpg">
* Drux (N) <img src="images/MS3188-N-Drux.jpg">
* Med (O) <img src="images/MS3188-O-Med.jpg">
* Mals (P) <img src="images/MS3188-P-Mals.jpg">
* Ger (Q) <img src="images/MS3188-Q-Ger.jpg">
* Don (R) <img src="images/MS3188-R-Don.jpg">
* Fam (S) <img src="images/MS3188-S-Fam.jpg">
* Gisa (T) <img src="images/MS3188-T-Gisa.jpg">
* Van (U) <img src="images/MS3188-U-Van.jpg">
* Pal (X) <img src="images/MS3188-X-Pal.jpg">
* Ceph (Z) <img src="images/MS3188-Z-Ceph.jpg">

These images were then loaded into fontforge, and the EnochianOpen.sfd and
EnochianOpen.otf files were produced without reference to any other font data
(i.e., just the images). This font is licensed under the SIL Open Font
License: http://scripts.sil.org/OFL

## Comparison

The excellent Esoterik font, from Kenneth Hirst, was used for comparison after
the fontforge work was completed. I.e., nothing in the font design for
EnochianOpen is based off Esoterik -- EnochianOpen derives purely from the
Sloane MS 3188 images.

For more information about Esoterik, please see
https://sites.google.com/site/cosmoramaent/home/download (This font is
shareware and I have purchased a license for it.)

The comparison document is [here](demo.pdf)

